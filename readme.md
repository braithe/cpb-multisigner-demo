#cpb-multisigner-demo
This is a camunda spring boot application that is configured to connect to a local instance of MySQL 5.6 containing a databse named multisigner.
You can configure the datasource in the application.yml, and change these settings if needed.

#features
This app demonstrates a few things about spring boot with camunda:
1. how to setup a spring boot camunda project
2. what properties need to be defined in the .bpmn file for it to be executible
3. how to setup properties for a xor gate to behave properly
4. how process variables work
5. how to configure the datasource to connect to a local or remote MySQL database
6. how human tasks work/behave
7. how service tasks work/behave
