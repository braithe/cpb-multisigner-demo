package com.citi.cpb.gft.multisigner.service;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class ApproversService implements JavaDelegate {

    public void execute(DelegateExecution execution) throws Exception {
        execution.setVariable("approverAccountId", "1234");
    }
}
