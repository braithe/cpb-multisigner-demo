package com.citi.cpb.gft.multisigner.service;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendApprovalMessageService implements JavaDelegate {

    private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);

    public void execute(DelegateExecution execution) throws Exception {
        String approverAccountId = (String) execution.getVariable("approverAccountId");
        logger.error("Approved notification sent to account: " + approverAccountId);
    }
}