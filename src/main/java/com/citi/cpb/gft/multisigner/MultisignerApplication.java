package com.citi.cpb.gft.multisigner;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableProcessApplication
public class MultisignerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultisignerApplication.class, args);
	}
}
